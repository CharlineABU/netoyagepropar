/**
 * POJO du projet
 */
package pojo;

/**
 * Objet Senior qui herite de Employe
 * 
 * @author Charline
 *
 */
public class Senior extends Employe {
    /**
     * Nombre d'operation prise
     */
    private int        nombreOperationPrise;
    /**
     * Compteur static permettant l'auto-incr�mentation de le nombre d'op�ration prise
     */
    public static int compteurNombreOperation;

    /**
     * Constructeur par defaut
     */
    public Senior() {
        super();
    }

    /**
     * Constructeur : permet d'instancier un senior avec son email et son mot de passe
     * 
     * @param nombreOperationPrise d'un senior
     */
    public Senior(final int nombreOperationPrise) {
        
        this.nombreOperationPrise = nombreOperationPrise;
    }

    /**
     * Permet d'initialiser un Senior
     * 
     * @param id d'un senior
     * @param nom d'un senior
     * @param prenom d'un senior
     * @param categorie d'un senior
     * @param email d'un senior
     * @param motDePasse d'un senior
     * @return senior intanci�
     */
    public static Senior initSenior(final int id, final String nom, final String prenom, final String categorie, final String email, final String motDePasse) {
        Senior senior = new Senior();
        senior.setId(id);
        senior.setNom(nom);
        senior.setPrenom(prenom);
        senior.setCategorie(categorie);
        senior.setEmail(email);
        senior.setMotDePasse(motDePasse);
        return senior;
    }

    /**
     * Permet d'initaliser un Senior avec info employ�
     * 
     * @param employe
     * @return senior initialis�
     */
    public static Senior initSenior(final Employe employe) {
        Senior senior = new Senior();
        senior.setId(employe.getId());
        senior.setNom(employe.getNom());
        senior.setPrenom(employe.getPrenom());
        senior.setCategorie(employe.getCategorie());
        senior.setEmail(employe.getEmail());
        senior.setMotDePasse(employe.getMotDePasse());
        return senior;
    }

    @Override
    public String toString() {
        return super.toString()+"Senior [nombreOperationPrise=" + nombreOperationPrise + "]";
    }

    /**
     * @return the nombreOperationPrise
     */
    public int getNombreOperationPrise() {
        return nombreOperationPrise;
    }

    /**
     * @param nombreOperationPrise the nombreOperationPrise to set
     */
    public void setNombreOperationPrise(final int nombreOperationPrise) {
        this.nombreOperationPrise = nombreOperationPrise;
    }

    /**
     * @return the compteurNombreOperation
     */
    public static int getCompteurNombreOperation() {
        return compteurNombreOperation;
    }

    /**
     * @param compteurNombreOperation the compteurNombreOperation to set
     */
    public static void setCompteurNombreOperation(final int compteurNombreOperation) {
        Senior.compteurNombreOperation = compteurNombreOperation;
    }

}

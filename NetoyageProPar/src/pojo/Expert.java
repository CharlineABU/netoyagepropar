/**
 * POJO du projet
 */
package pojo;

/**
 * Objet Expert qui herite de Employe
 * 
 * @author Charline
 *
 */
public class Expert extends Employe {
	/**
	 * Nombre d'operation prise
	 */
	private int nombreOperationPrise;
	/**
	 * Compteur static permettant l'auto-incrémentation de le nombre d'opération
	 * prise
	 */
	public static int compteurNombreOperation;

	/**
	 * Constructeur par defaut
	 */
	public Expert() {
		super();
	}

	/**
	 * Constructeur : permet d'instancier un expert avec son email et son mot de
	 * passe
	 * 
	 * @param nombreOperationPrise d'un expert
	 */
	public Expert(final int nombreOperationPrise) {
		this.nombreOperationPrise = compteurNombreOperation;
	}

	/**
	 * Permet d'initialiser un expert
	 * 
	 * @param id
	 * @param nom
	 * @param prenom
	 * @param categorie
	 * @param email
	 * @param motDePasse
	 * @return
	 */
	public static Expert initExpert(final int id, final String nom, final String prenom, final String categorie,
			final String email, final String motDePasse) {
		Expert expert = new Expert();
		expert.setId(id);
		expert.setNom(nom);
		expert.setPrenom(prenom);
		expert.setCategorie(categorie);
		expert.setEmail(email);
		expert.setMotDePasse(motDePasse);
		return expert;
	}
	
	 /**
     * Permet d'initaliser un Expert avec info employé
     * 
     * @param employe
     * @return expert initialisé
     */
    public static Expert initExpert(final Employe employe) {
        Expert expert = new Expert();
        expert.setId(employe.getId());
        expert.setNom(employe.getNom());
        expert.setPrenom(employe.getPrenom());
        expert.setCategorie(employe.getCategorie());
        expert.setEmail(employe.getEmail());
        expert.setMotDePasse(employe.getMotDePasse());
        return expert;
    }

	@Override
	public String toString() {
		return super.toString()+"Expert [nombreOperationPrise=" + nombreOperationPrise + "]";
	}

	/**
	 * @return the nombreOperationPrise
	 */
	public int getNombreOperationPrise() {
		return nombreOperationPrise;
	}

	/**
	 * @param nombreOperationPrise the nombreOperationPrise to set
	 */
	public void setNombreOperationPrise(int nombreOperationPrise) {
		this.nombreOperationPrise = nombreOperationPrise;
	}

	/**
	 * @return the compteurNombreOperation
	 */
	public static int getCompteurNombreOperation() {
		return compteurNombreOperation;
	}

	/**
	 * @param compteurNombreOperation the compteurNombreOperation to set
	 */
	public static void setCompteurNombreOperation(int compteurNombreOperation) {
		Expert.compteurNombreOperation = compteurNombreOperation;
	}

}

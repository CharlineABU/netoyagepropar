/**
 * POJO du projet
 */
package pojo;

import java.util.ArrayList;

/**
 * Objet Client qui herite de Personne
 * 
 * @author Charline
 *
 */
public class Client extends Personne {

    private ArrayList<Operation> listeDesOperationsTraiteParSociete = new ArrayList<Operation>();

    /**
     * Constructeur par defaut
     */
    public Client() {
        super();
    }

    /**
     * Constructeur : permet d'instancier un client avec ses attributs
     * 
     * @param id
     * @param nom
     * @param prenom
     * @param categorie
     */
    public Client(int id, String nom, String prenom, String categorie) {
        super(id, nom, prenom, categorie);
        // TODO Auto-generated constructor stub
    }

    /**
     * Permet d'initaliser un Client avec info personne
     * 
     * @param personne
     * @return client initialisť
     */
    public static Client initClient(final Personne personne) {
        Client client = new Client();
        client.setId(personne.getId());
        client.setNom(personne.getNom());
        client.setPrenom(personne.getPrenom());
        client.setCategorie(personne.getCategorie());
        return client;
    }

    @Override
    public String toString() {
        return super.toString()+ "Client [listeDesOperationsTraiteParSociete=" + listeDesOperationsTraiteParSociete;
    }

    /**
     * @return the listeDesOperationsTraiteParSociete
     */
    public ArrayList<Operation> getListeDesOperationsTraiteParSociete() {
        return listeDesOperationsTraiteParSociete;
    }

    /**
     * @param listeDesOperationsTraiteParSociete the listeDesOperationsTraiteParSociete to set
     */
    public void setListeDesOperationsTraiteParSociete(final ArrayList<Operation> listeDesOperationsTraiteParSociete) {
        this.listeDesOperationsTraiteParSociete = listeDesOperationsTraiteParSociete;
    }
    
    
    
    

}

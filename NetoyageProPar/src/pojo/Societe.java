/**
 * POJO du projet
 */
package pojo;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Objet Societe
 * 
 * @author Charline
 *
 */
public class Societe {

    /**
     * Identifiant de la soci�te
     */
    private int                        idSociete;
    /**
     * Nom de la societe
     */
    private String                     nom;
    /**
     * Liste de toutes les employ�s
     */
    public static ArrayList<Employe>   listeDesEmployes                = new ArrayList<Employe>();
    /**
     * Liste de tous les clients qui ont fait trait�es une operation dans la societe
     */
    public static ArrayList<Client>    listeDesClients                 = new ArrayList<Client>();
    /**
     * Liste de toutes les op�rations en cours
     */
    public static ArrayList<Operation> listeDesOperationsEnCours       = new ArrayList<Operation>();
    /**
     * Liste de toutes les op�tations termin�es
     */
    public static ArrayList<Operation> listeDesOperationsTerminee      = new ArrayList<Operation>();
    /**
     * Chiffre d'affaires des op�rations termin�es
     */
    public static double               chiffreAffaire                  = 0;
    /**
     * Chiffre d'affaires des op�rations en cours
     */
    public static double               paiementEnAttenteDeFinDeTravaux = 0;

    /**
     * Constructeur par defaut
     */
    public Societe() {
        super();
    }

    /**
     * @param idSociete
     * @param nom
     */
    public Societe(final int idSociete, final String nom) {
        super();
        this.idSociete = idSociete;
        this.nom = nom;
    }

    /**
     * Permet d'afficher une liste d'operation tri� par noms des clients
     * 
     * @param clients
     * @return
     */
    public static ArrayList<Operation> afficheListeDesNomsOperationsOrderByName(final ArrayList<Operation> operations) {
        ArrayList<String> listeDesNomsClients = new ArrayList<String>();
        for (Operation operation : operations) {
            listeDesNomsClients.add(operation.getClient().getNom());
        }
        listeDesNomsClients.sort(String::compareToIgnoreCase);
        ArrayList<Operation> listeTriee = new ArrayList<Operation>();
        for (String nom : listeDesNomsClients) {
            for (int i = 0; i < operations.size(); i++) {
                if (nom.equals(operations.get(i).getClient().getNom())) {
                    listeTriee.add(operations.get(i));
                }
            }
        }
        
        return listeTriee;
    }

    @Override
    public String toString() {
        return "Societe \n   id Societe :\t <" + idSociete + ">\n   Nom Societe : " + nom + "\n   Liste des Employes : " + Societe.listeDesEmployes.toString() + "\n   Liste des Clients : "
                        + Societe.listeDesClients.toString() + "\n   Liste des Operations en cours : " + Societe.listeDesOperationsEnCours.toString() + "\n   Liste des Operations termin�es : "
                        + Societe.listeDesOperationsTerminee.toString() + "\n   Chiffre d'Affaire : " + Societe.chiffreAffaire + "\n   Paiement en attente de fin de travaux : "
                        + Societe.paiementEnAttenteDeFinDeTravaux;
    }

    /**
     * @return the idSociete
     */
    public int getIdSociete() {
        return idSociete;
    }

    /**
     * @param idSociete the idSociete to set
     */
    public void setIdSociete(final int idSociete) {
        this.idSociete = idSociete;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(final String nom) {
        this.nom = nom;
    }

    /**
     * @return the listeDesEmployes
     */
    public static ArrayList<Employe> getListeDesEmployes() {
        return listeDesEmployes;
    }

    /**
     * @param listeDesEmployes the listeDesEmployes to set
     */
    public static void setListeDesEmployes(final ArrayList<Employe> listeDesEmployes) {
        Societe.listeDesEmployes = listeDesEmployes;
    }

    /**
     * @return the listeDesClients
     */
    public static ArrayList<Client> getListeDesClients() {
        return listeDesClients;
    }

    /**
     * @param listeDesClients the listeDesClients to set
     */
    public static void setListeDesClients(final ArrayList<Client> listeDesClients) {
        Societe.listeDesClients = listeDesClients;
    }

    /**
     * @return the listeDesOperationsEnCours
     */
    public static ArrayList<Operation> getListeDesOperationsEnCours() {
        return listeDesOperationsEnCours;
    }

    /**
     * @param listeDesOperationsEnCours the listeDesOperationsEnCours to set
     */
    public static void setListeDesOperationsEnCours(final ArrayList<Operation> listeDesOperationsEnCours) {
        Societe.listeDesOperationsEnCours = listeDesOperationsEnCours;
    }

    /**
     * @return the listeDesOperationsTerminee
     */
    public static ArrayList<Operation> getListeDesOperationsTerminee() {
        return listeDesOperationsTerminee;
    }

    /**
     * @param listeDesOperationsTerminee the listeDesOperationsTerminee to set
     */
    public static void setListeDesOperationsTerminee(final ArrayList<Operation> listeDesOperationsTerminee) {
        Societe.listeDesOperationsTerminee = listeDesOperationsTerminee;
    }

    /**
     * @return the chiffreAffaire
     */
    public static double getChiffreAffaire() {
        return chiffreAffaire;
    }

    /**
     * @param chiffreAffaire the chiffreAffaire to set
     */
    public static void setChiffreAffaire(final double chiffreAffaire) {
        Societe.chiffreAffaire = chiffreAffaire;
    }

}

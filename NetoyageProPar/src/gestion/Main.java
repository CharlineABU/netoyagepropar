/**
 * 
 */
package gestion;

import java.util.Scanner;

import pojo.Apprenti;
import pojo.Client;
import pojo.Employe;
import pojo.Expert;
import pojo.Operation;
import pojo.Senior;
import pojo.Societe;

/**
 * Class principale qui permet d'executer le projet
 * 
 * @author Charline
 *
 */
public class Main {

    public static class MenuAffichagePropar {

        //finir initSociete
        /**
         * Permet de cr�er une societe test
         * 
         * @param societe � cr�er
         */
        public static void initSociete(final Societe societe) {

            Employe expert1 = Expert.initExpert(01, "toto", "to", "E", "toto@gmail.com", "toto");
            Employe senior1 = Senior.initSenior(02, "titi", "ti", "S", "titi@gmail.com", "titi");
            Employe apprenti1 = Apprenti.initApprenti(03, "tata", "ta", "A", "tata@gmail.com", "tata");

            Client client1 = new Client(23, "Zayard", "Rodolphe", "C");
            Client client2 = new Client(24, "Chevalier", "Nestor", "C");
            Client client3 = new Client(25, "Saly", "Abdalah", "C");

            Operation ope1 = new Operation("Peinture", client1, "G", "EC");
            Operation ope2 = new Operation("Menage", client1, "M", "TER");
            Operation ope3 = new Operation("Carrelage", client2, "P", "EC");
            Operation ope4 = new Operation("Jardinage", client3, "G", "TER");

            client1.getListeDesOperationsTraiteParSociete().add(ope1);
            client1.getListeDesOperationsTraiteParSociete().add(ope2);
            client2.getListeDesOperationsTraiteParSociete().add(ope3);
            client3.getListeDesOperationsTraiteParSociete().add(ope4);

            Societe.listeDesEmployes.add(apprenti1);
            Societe.listeDesEmployes.add(senior1);
            Societe.listeDesEmployes.add(expert1);
            Societe.listeDesClients.add(client1);
            Societe.listeDesClients.add(client2);
            Societe.listeDesClients.add(client3);

            PrendreOperation.prendreOperation(expert1, ope1);
            PrendreOperation.prendreOperation(senior1, ope3);
            PrendreOperation.prendreOperation(apprenti1, ope2);
            PrendreOperation.prendreOperation(senior1, ope4);

        }

        private static void affichageMenuLancement() {

            System.out.println();
            System.out.println("-------------------------------    MENU CONNEXION PROPAR   ------------------------------------------------");

            System.out.println("A- Se connecter");
            System.out.println("B- Afficher la liste des op�ations en cours");
            System.out.println("C- Afficher la liste des op�rations termin�es");
            System.out.println("Q- Quitter");
            System.out.println();
            System.out.println("Veuillez saisir une lettre majuscule afin de choisir l'option souhait�e");

        }

        /**
         * @param args
         */
        public static void main(final String[] args) {
            Societe societe = new Societe(58422, "Netoyage ProPar");
            initSociete(societe);
            //System.out.println(societe);
            System.out.println(Societe.afficheListeDesNomsOperationsOrderByName(societe.getListeDesOperationsEnCours()));
            //			affichageMenuLancement();
            //
            //			String lettreChoisie;
            //
            //			do {
            //				Scanner clavier = new Scanner(System.in);
            //
            //				lettreChoisie = clavier.nextLine();
            //				switch (lettreChoisie) {
            //				case "A":
            //
            //					System.out.println("Saisissez votre identifiant de connexion");
            //					String login = clavier.nextLine();
            //					System.out.println("Saisissez votre mot de passe");
            //					String motDePasse = clavier.nextLine();
            //					if (login.equals("Admin") && motDePasse.equals("PassWord")) {
            //						affichageMenu();
            //						////
            //						////
            //					} else {
            //						affichageMenuLancement();
            //						System.out.println("Donn�es saisies erron�es, appuyez sur A pour recommencer la saisie ou ");
            //						break;
            //					}
            //
            //				case "B":
            //
            //					break;
            //				case "C":
            //
            //				case "Q":
            //					System.exit(0);
            //					break;
            //
            //				default:
            //					System.out.println(
            //							"Saisie erron�e, veuillez saisir une lettre majuscule afin de choisir l'option souhait�e");
            //				}
            //
            //			} while (!lettreChoisie.equals("Q"));

        }

        private static void affichageMenu() {
            System.out.println("-------------------------------    MENU CONNEXION PROPAR   ------------------------------------------------");
            System.out.println();
            System.out.println("A- Ajouter un employ�");
            System.out.println("B- Consulter le chiffre d'affaire");
            System.out.println("C- Consulter la liste des op�rations en cours");
            System.out.println("D- Consulter la liste des op�rations termin�es");
            System.out.println("E- Cr�er une op�ration");
            System.out.println("Q- Quitter");
            System.out.println();
            Scanner clavier = new Scanner(System.in);
            System.out.println("Veuillez saisir une lettre majuscule afin de choisir l'option souhait�e");
        }
    }

}

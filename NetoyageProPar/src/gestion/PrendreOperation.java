/**
 * Gestion du projet
 */
package gestion;

import pojo.Apprenti;
import pojo.Employe;
import pojo.Expert;
import pojo.Operation;
import pojo.Senior;
import pojo.Societe;

/**
 * Class Association entre Senior/Expert et Operation
 * 
 * @author Charline
 *
 */
public class PrendreOperation {

    /**
     * Identifiant de PrendreOperation
     */
    private int              id;
    /**
     * Compteur static permettant l'auto-incr�mentation de l'identifiant
     */
    public static int        compteurId;
    /**
     * Une instance d'un expert
     */
    private Employe          expert                      = new Expert();
    /**
     * Une instance d'un senior
     */
    private Employe          senior                      = new Senior();
    /**
     * Une instance d'un apprenti
     */
    private Employe          apprenti                    = new Apprenti();

    /**
     * Une instance d'une operation
     */
    private Operation        operation;

    /**
     * Nombre d'op�rations maximum que peut prendre un senior
     */
    private final static int NOMBRE_OPERATION_MAX_SENIOR = 3;
    /**
     * Nombre d'op�rations maximum que peut prendre un expert
     */
    private final static int NOMBRE_OPERATION_MAX_EXPERT = 5;

    /**
     * Constructeur par defaut
     */
    public PrendreOperation() {
        PrendreOperation.compteurId++;
    }

    /**
     * Constructeur : permet d'instancier prise d'op�ration avec attributs
     * 
     * @param expert qui prend l'operation
     * @param senior qui prend l'operation
     * @param operation prise
     */
    public PrendreOperation(final Employe expert, final Employe senior, final Operation operation) {
        super();
        this.expert = expert;
        this.senior = senior;
        this.operation = operation;
    }

    /**
     * Permet � un user de prendre une operation
     * 
     * @param user connect�
     * @param operation � prendre
     * @return prendreOperation information de la prise d'operation
     */
    public static PrendreOperation prendreOperation(final Employe user, final Operation operation) {
        PrendreOperation prendreOperation = new PrendreOperation();

        prendreOperation.setOperation(operation);

        if (user instanceof Senior) {
            prendreOperation.setSenior(Senior.initSenior(user));
            Senior.compteurNombreOperation++;
            ((Senior) user).setNombreOperationPrise(Senior.compteurNombreOperation);
        }
        if (user instanceof Expert) {
            prendreOperation.setExpert(Expert.initExpert(user));
            Expert.compteurNombreOperation++;
            ((Expert) user).setNombreOperationPrise(Expert.compteurNombreOperation);
        }
        if (user instanceof Apprenti) {
            prendreOperation.setApprenti(Apprenti.initApprenti(user));
            Apprenti.compteurNombreOperation++;
            ((Apprenti) user).setNombreOperationPrise(Apprenti.compteurNombreOperation);
        }
        if (operation.getStatut().equals("EC")) {
            Societe.getListeDesOperationsEnCours().add(operation);
        }
        if (operation.getStatut().equals("TER")) {
            Societe.getListeDesOperationsTerminee().add(operation);
        }
        if (operation.getStatut().equals("TER")) {
            Societe.chiffreAffaire += operation.getPrix();
        } else {
            Societe.paiementEnAttenteDeFinDeTravaux += operation.getPrix();
        }

        return prendreOperation;
    }

    /**
     * Permet de terminer une op�ration qui �tait en cours
     * 
     * @param user qui termine l'operation
     * @param operation � terminer
     */
    public static void terminerOperation(final Employe user, final Operation operation) {
        operation.setStatut("TER");
        Societe.chiffreAffaire += operation.getPrix();
        Societe.paiementEnAttenteDeFinDeTravaux -= operation.getPrix();
    }

    @Override
    public String toString() {
        return "PrendreOperation [id=" + id + ", expert=" + expert + ", senior=" + senior + ", operation=" + operation + "]";
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * @return the compteurId
     */
    public static int getCompteurId() {
        return compteurId;
    }

    /**
     * @param compteurId the compteurId to set
     */
    public static void setCompteurId(final int compteurId) {
        PrendreOperation.compteurId = compteurId;
    }

    /**
     * @return the expert
     */
    public Employe getExpert() {
        return expert;
    }

    /**
     * @param expert the expert to set
     */
    public void setExpert(final Employe expert) {
        this.expert = expert;
    }

    /**
     * @return the senior
     */
    public Employe getSenior() {
        return senior;
    }

    /**
     * @param senior the senior to set
     */
    public void setSenior(final Employe senior) {
        this.senior = senior;
    }

    /**
     * @return the apprenti
     */
    public Employe getApprenti() {
        return apprenti;
    }

    /**
     * @param apprenti the apprenti to set
     */
    public void setApprenti(final Employe apprenti) {
        this.apprenti = apprenti;
    }

    /**
     * @return the operation
     */
    public Operation getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(final Operation operation) {
        this.operation = operation;
    }

    /**
     * @return the nombreOperationMaxSenior
     */
    public static int getNombreOperationMaxSenior() {
        return NOMBRE_OPERATION_MAX_SENIOR;
    }

    /**
     * @return the nombreOperationMaxExpert
     */
    public static int getNombreOperationMaxExpert() {
        return NOMBRE_OPERATION_MAX_EXPERT;
    }

}

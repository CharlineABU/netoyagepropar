/**
 * Exceptions du projet
 */
package exceptions;

/**
 * Classe qui permet de lever les Exceptions issues de la gestion des op�rations
 * 
 * @author Charline
 *
 */
public class ExceptionsGestionDesOperations extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 5540250549399359855L;

    /**
     * Constructeur par defaut
     */
    public ExceptionsGestionDesOperations() {
        super();
    }

    /**
     * permet de lever une exception avec un message descriptif � afficher
     * 
     * @param message � afficher
     */
    public ExceptionsGestionDesOperations(final String message) {
        super(message);
    }

    /**
     * permet de lever une exception avec une cause
     * 
     * @param cause de l'exception
     */
    public ExceptionsGestionDesOperations(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    /**
     * permet de lever une exception avec un message � afficher et une cause
     * 
     * @param message � afficher
     * @param cause de l'exception
     */
    public ExceptionsGestionDesOperations(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * permet de lever une excetion avec option
     * 
     * @param message � afficher
     * @param cause de l'exception
     * @param enableSuppression option de suppression
     * @param writableStackTrace option d'ecriture
     */
    public ExceptionsGestionDesOperations(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}

/**
 * Gestion du projet
 */
package gestion;

import java.util.ArrayList;

import pojo.Apprenti;
import pojo.Client;
import pojo.Employe;
import pojo.Expert;
import pojo.Operation;
import pojo.Senior;
import pojo.Societe;

/**
 * Class Association entre Senior/Expert et Operation
 * 
 * @author Charline
 *
 */
public class PrendreOperation {

    /**
     * Identifiant de PrendreOperation
     */
    private int              id;
    /**
     * Compteur static permettant l'auto-incr�mentation de l'identifiant
     */
    public static int        compteurId;
    /**
     * Une instance d'un expert
     */
    private Employe          expert                      = new Expert();
    /**
     * Une instance d'un senior
     */
    private Employe          senior                      = new Senior();
    /**
     * Une instance d'un apprenti
     */
    private Employe          apprenti                    = new Apprenti();
    /**
     * Une instance d'une operation
     */
    private Operation        operation                   = new Operation();

    /**
     * Nombre d'op�rations maximum que peut prendre un senior
     */
    private final static int NOMBRE_OPERATION_MAX_SENIOR = 3;
    /**
     * Nombre d'op�rations maximum que peut prendre un expert
     */
    private final static int NOMBRE_OPERATION_MAX_EXPERT = 5;

    /**
     * Constructeur par defaut
     */
    public PrendreOperation() {
        PrendreOperation.compteurId++;
    }

    /**
     * Constructeur : permet d'instancier prise d'op�ration avec attributs
     * 
     * @param expert qui prend l'operation
     * @param senior qui prend l'operation
     * @param operation prise
     */
    public PrendreOperation(final Employe expert, final Employe senior, final Operation operation) {
        super();
        this.expert = expert;
        this.senior = senior;
        this.operation = operation;
    }

    /**
     * Permet � un user de prendre une operation
     * 
     * @param user connect�
     * @param operation � prendre
     * @return prendreOperation information de la prise d'operation
     */
    public static PrendreOperation prendreOperation(final Employe employe, final Operation operation) {

        PrendreOperation prendreOperation = new PrendreOperation();
        Employe expert = new Expert();
        Employe senior = new Senior();
        Employe apprenti = new Apprenti();

        if (employe.getCategorie().equals("E")) {
            if (NOMBRE_OPERATION_MAX_EXPERT == employe.getNombreOperationPrise()) {
                System.out.println("Vous ne pouvez plus pendre d'operation !");
            } else {
                expert = Expert.initExpert(employe);
                prendreOperation.setExpert(Expert.initExpert(expert));
                expert.setCompteurNombreOperation(expert.getCompteurNombreOperation() + 1);
                expert.setNombreOperationPrise(expert.getCompteurNombreOperation());
                System.out.println("Vous avez " + expert.getNombreOperationPrise() + " op�ration(s) prise!");
                System.out.println("Vous ne pouvez prendre que " + NOMBRE_OPERATION_MAX_EXPERT);
                validerEnregistrement(operation, prendreOperation);

            }
        } else if (employe.getCategorie().equals("S")) {
            if (NOMBRE_OPERATION_MAX_SENIOR == employe.getNombreOperationPrise()) {
                System.out.println("Vous ne pouvez plus pendre d'operation !");
            } else {
                senior = Senior.initSenior(employe);
                prendreOperation.setSenior(Senior.initSenior(senior));
                senior.setCompteurNombreOperation(senior.getCompteurNombreOperation() + 1);
                senior.setNombreOperationPrise(senior.getCompteurNombreOperation());
                System.out.println("Vous avez " + senior.getNombreOperationPrise() + " op�ration(s) prise!");
                System.out.println("Vous ne pouvez prendre que " + NOMBRE_OPERATION_MAX_SENIOR);
                validerEnregistrement(operation, prendreOperation);
            }

        } else if (employe.getCategorie().equals("A")) {
            if (employe.getNombreOperationPrise() == 1) {
                System.out.println("Vous ne pouvez plus pendre d'operation !");
            } else {
                apprenti = Apprenti.initApprenti(employe);
                prendreOperation.setApprenti(Apprenti.initApprenti(apprenti));
                apprenti.setCompteurNombreOperation(apprenti.getCompteurNombreOperation() + 1);
                apprenti.setNombreOperationPrise(apprenti.getCompteurNombreOperation());
                System.out.println("Vous avez " + apprenti.getNombreOperationPrise() + " op�ration(s) prise!");
                validerEnregistrement(operation, prendreOperation);
            }
        }
        return prendreOperation;
    }

    /**
     * Permet de valider l'enregistrement d'une operation
     * 
     * @param operation
     * @param prendreOperation
     */
    public static void validerEnregistrement(final Operation operation, PrendreOperation prendreOperation) {
        prendreOperation.setOperation(operation);

        if (operation.getStatut().equals("EC")) {
            Societe.getListeDesOperationsEnCours().add(operation);
        }
        if (operation.getStatut().equals("TER")) {
            Societe.getListeDesOperationsTerminee().add(operation);
        }
        if (operation.getStatut().equals("TER")) {
            Societe.chiffreAffaire += operation.getPrix();
        } else {
            Societe.paiementEnAttenteDeFinDeTravaux += operation.getPrix();
        }
        Societe.listeDesOperationsEnregistrees.add(prendreOperation);

        System.out.println("Operation enregistr�e !");
    }

    /**
     * Permet de lister toutes les op�rations d'un user � partie de son id
     * 
     * @param id
     * @return listOperationUser liste de toute les operations
     */
    public static ArrayList<Operation> findOperationUserById(final Employe user, final ArrayList<Operation> operationEnCours, final ArrayList<Operation> operationTerminee) {
        ArrayList<Operation> listOperationUser = new ArrayList<Operation>();

        for (Operation op : Societe.listeDesOperationsEnCours) {
            if (user.getId() == op.getEmploye().getId()) {
                listOperationUser.add(op);
            }
        }
        for (Operation op : Societe.listeDesOperationsTerminee) {
            if (user.getId() == op.getEmploye().getId()) {
                listOperationUser.add(op);
            }
        }

        return listOperationUser;
    }

    /**
     * Permet de lister les operations termin�es d'un user
     * 
     * @param user
     * @return
     */
    public static ArrayList<Operation> findOperationTerUserById(final Employe user) {
        ArrayList<Operation> listOperationUser = findOperationUserById(user, Societe.listeDesOperationsEnCours, Societe.listeDesOperationsTerminee);
        ArrayList<Operation> listOperationTerUser = new ArrayList<Operation>();

        for (Operation operation : listOperationUser) {
            if (operation.getStatut().equals("TER")) {
                listOperationTerUser.add(operation);
            }
        }

        return listOperationTerUser;
    }

    /**
     * Permet de lister les operations en cours d'un user
     * 
     * @param user
     * @return
     */
    public static ArrayList<Operation> findOperationEnCoursUserById(final Employe user) {
        ArrayList<Operation> listOperationUser = findOperationUserById(user, Societe.listeDesOperationsEnCours, Societe.listeDesOperationsTerminee);
        ArrayList<Operation> listOperationEnCoursUser = new ArrayList<Operation>();
        for (Operation operation : listOperationUser) {
            if (operation.getStatut().equals("EC")) {
                listOperationEnCoursUser.add(operation);
            }
        }

        return listOperationEnCoursUser;
    }

    /**
     * Permet de trouver une operation � partir de son Id
     * 
     * @param id de l'operation � chercher
     * @return l'operation � chercher
     */
    public static Operation findOperationById(final Integer id) {
        for (Operation operation : Societe.listeDesOperationsEnCours) {
            if (operation.getIdOperation() == id) {
                return operation;
            }
        }
        for (Operation operation : Societe.listeDesOperationsTerminee) {
            if (operation.getIdOperation() == id) {
                return operation;
            }
        }
        return null;
    }

    /**
     * Permet de terminer une op�ration qui �tait en cours
     * 
     * @param user qui termine l'operation
     * @param operation � terminer
     */
    public static void terminerOperation(final Employe user, final Operation operation) {
        operation.setStatut("TER");
        Societe.chiffreAffaire += operation.getPrix();
        Societe.paiementEnAttenteDeFinDeTravaux -= operation.getPrix();
        for (Operation ope : Societe.listeDesOperationsEnCours) {
            if (ope.getIdOperation() == operation.getIdOperation()) {
                Societe.listeDesOperationsEnCours.remove(operation);
                Societe.listeDesOperationsTerminee.add(operation);
            }
        }
        for (PrendreOperation prendreOpe : Societe.listeDesOperationsEnregistrees) {
            if (prendreOpe.getOperation().getIdOperation() == operation.getIdOperation()) {
                prendreOpe.getOperation().setStatut("TER");
            }
        }

    }

    /**
     * permet de verifier si un client est d�ja enregistr�
     * 
     * @param nom du client
     * @param prenom client
     * @return client si existe sinon null
     */
    public static Client ifClientExist(final String nom, final String prenom) {
        // Client client = new Client();
        for (Client client : Societe.listeDesClients) {
            if (client.getNom().equals(nom) && client.getPrenom().equals(prenom)) {
                return client;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "PrendreOperation [id=" + id + ", expert=" + expert + ", senior=" + senior + ", operation=" + operation + "]";
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * @return the compteurId
     */
    public static int getCompteurId() {
        return compteurId;
    }

    /**
     * @param compteurId the compteurId to set
     */
    public static void setCompteurId(final int compteurId) {
        PrendreOperation.compteurId = compteurId;
    }

    /**
     * @return the expert
     */
    public Employe getExpert() {
        return expert;
    }

    /**
     * @param expert the expert to set
     */
    public void setExpert(final Employe expert) {
        this.expert = expert;
    }

    /**
     * @return the senior
     */
    public Employe getSenior() {
        return senior;
    }

    /**
     * @param senior the senior to set
     */
    public void setSenior(final Employe senior) {
        this.senior = senior;
    }

    /**
     * @return the apprenti
     */
    public Employe getApprenti() {
        return apprenti;
    }

    /**
     * @param apprenti the apprenti to set
     */
    public void setApprenti(final Employe apprenti) {
        this.apprenti = apprenti;
    }

    /**
     * @return the operation
     */
    public Operation getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(final Operation operation) {
        this.operation = operation;
    }

    /**
     * @return the nombreOperationMaxSenior
     */
    public static int getNombreOperationMaxSenior() {
        return NOMBRE_OPERATION_MAX_SENIOR;
    }

    /**
     * @return the nombreOperationMaxExpert
     */
    public static int getNombreOperationMaxExpert() {
        return NOMBRE_OPERATION_MAX_EXPERT;
    }

}

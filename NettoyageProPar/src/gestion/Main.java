/**
 * 
 */
package gestion;

import java.util.ArrayList;

import pojo.Apprenti;
import pojo.Client;
import pojo.Employe;
import pojo.Expert;
import pojo.Operation;
import pojo.Senior;
import pojo.Societe;

/**
 * Class principale qui permet d'executer le projet
 * 
 * @author Charline
 *
 */
public class Main {

    public static class MenuAffichagePropar {

        // finir initSociete
        /**
         * Permet de cr�er une societe test
         * 
         * @param societe � cr�er
         */
        public static void initSociete(final Societe societe) {

            Employe expert1 = Expert.initExpert(01, "toto", "to", "E", "toto@gmail.com", "toto");
            Employe senior1 = Senior.initSenior(02, "titi", "ti", "S", "titi@gmail.com", "titi");
            Employe apprenti1 = Apprenti.initApprenti(03, "tata", "ta", "A", "tata@gmail.com", "tata");

            Client client1 = new Client(23, "Rousseau", "Rodolphe", "C");
            Client client2 = new Client(24, "Chevalier", "Nestor", "C");
            Client client3 = new Client(25, "Saly", "Abdalah", "C");
            Client client4 = new Client(25, "Tison", "Salim", "C");
            Client client5 = new Client(25, "Fettih", "Brahim", "C");

            Operation ope1 = new Operation("Peinture", client1, senior1, "G", "EC");
            Operation ope2 = new Operation("Menage", client1, apprenti1, "M", "TER");
            Operation ope3 = new Operation("Carrelage", client2, senior1, "P", "EC");
            Operation ope4 = new Operation("Jardinage", client3, expert1, "G", "TER");
            Operation ope5 = new Operation("Renovation parquet", client4, expert1, "P", "EC");
            Operation ope6 = new Operation("Menuiserie", client5, expert1, "G", "EC");

            client1.getListeDesOperationsTraiteParSociete().add(ope1);
            client1.getListeDesOperationsTraiteParSociete().add(ope2);
            client2.getListeDesOperationsTraiteParSociete().add(ope3);
            client3.getListeDesOperationsTraiteParSociete().add(ope4);
            client4.getListeDesOperationsTraiteParSociete().add(ope5);
            client5.getListeDesOperationsTraiteParSociete().add(ope6);

            Societe.listeDesEmployes.add(apprenti1);
            Societe.listeDesEmployes.add(senior1);
            Societe.listeDesEmployes.add(expert1);
            Societe.listeDesClients.add(client1);
            Societe.listeDesClients.add(client2);
            Societe.listeDesClients.add(client3);
            Societe.listeDesClients.add(client4);
            Societe.listeDesClients.add(client5);

            PrendreOperation.prendreOperation(expert1, ope4);
            PrendreOperation.prendreOperation(senior1, ope3);
            PrendreOperation.prendreOperation(apprenti1, ope2);
            PrendreOperation.prendreOperation(senior1, ope1);
            PrendreOperation.prendreOperation(expert1, ope5);
            PrendreOperation.prendreOperation(expert1, ope6);

        }

        /**
         * Permet d'afficher le menu d'acceuil
         */
        private static String affichageMenuLancement(final Societe societe) {

            System.out.println();
            System.out.println("-------------------------------    MENU " + societe.getNom() + " ACCEUIL  -------------------------------");

            System.out.println("A- Se connecter");
            System.out.println("B- Afficher la liste des op�ations en cours");
            System.out.println("C- Afficher la liste des op�rations termin�es");
            System.out.println("\nQ- Quitter");
            System.out.println();
            System.out.print("Veuillez saisir une lettre majuscule afin de choisir l'option souhait�e : ");
            String choix = ScannerUtil.lire();
            while (!choix.equals("A") && !choix.equals("B") && !choix.equals("C") && !choix.equals("Q")) {
                System.err.println("ERREUR : Veuillez saisir une lettre majuscule pour votre choix");
                choix = ScannerUtil.lire();
            }
            return choix;

        }

        /**
         * Permet d'afficher le menu Expert
         */
        private static String affichageMenuExpert(final Societe societe, final Employe user) {
            System.out.println("-------------------------------    MENU " + societe.getNom() + " ACCES EXPERT   -------------------------------");
            System.out.println();
            System.out.println("A- Ajouter un employ�");
            System.out.println("B- Consulter le chiffre d'affaire");
            System.out.println("C- Consulter la liste de mes op�rations termin�es");
            System.out.println("D- Consulter la liste de mes op�rations en cours");
            System.out.println("E- Prendre operation");
            System.out.println("F- Terminer operation");
            System.out.println("\nQ- Quitter");
            System.out.println();
            System.out.println("Veuillez saisir une lettre majuscule afin de choisir l'option souhait�e");
            String choix = ScannerUtil.lire();
            while (!choix.equals("A") && !choix.equals("B") && !choix.equals("C") && !choix.equals("D") && !choix.equals("E") && !choix.equals("F") && !choix.equals("Q")) {
                System.err.println("ERREUR : Veuillez saisir une lettre majuscule pour votre choix");
                choix = ScannerUtil.lire();
            }
            return choix;
        }

        /**
         * Permet d'afficher le menu Senior/Apprenti
         */
        private static String affichageMenuSeniorApprenti(final Societe societe, final Employe user) {
            System.out.println("-------------------------------    MENU " + societe.getNom() + " ACCES SENIOR/APPRENTI   -------------------------------");
            System.out.println();
            System.out.println("A- Prendre operation");
            System.out.println("B- Terminer operation");
            System.out.println("C- Consulter la liste de mes op�rations en cours");
            System.out.println("D- Consulter la liste de mes op�rations termin�es");
            System.out.println("\nQ- Quitter");
            System.out.println();
            System.out.println("Veuillez saisir une lettre majuscule afin de choisir l'option souhait�e");
            String choix = ScannerUtil.lire();
            while (!choix.equals("A") && !choix.equals("B") && !choix.equals("C") && !choix.equals("D") && !choix.equals("Q")) {
                System.err.println("ERREUR : Veuillez saisir une lettre majuscule pour votre choix");
                choix = ScannerUtil.lire();
            }
            return choix;
        }

        /**
         * Permet de chercher un employ� � partir de son mail et de son mdp
         *
         * @param email de l'employ� � chercher
         * @param mdp de l'employ� � chercher
         * @return l'employ� retrouv� si trouv� sinon on renvoi null
         */
        private static Employe findEmployeByEmailAndMdp(final String email, final String mdp) {
            for (Employe employe : Societe.listeDesEmployes) {
                if (email.equals(employe.getEmail()) && mdp.equals(employe.getMotDePasse())) {
                    return employe;
                }
            }
            return null;
        }

        /**
         * Gestion de la connection
         * 
         * @param societe du user
         * @param employe le user
         * @return
         */
        private static String connection(final Societe societe, final Employe employe) {
            Employe user = findEmployeByEmailAndMdp(employe.getEmail(), employe.getMotDePasse());
            System.out.println();
            String choix = "";
            try {
                if (employe.getEmail().equals(user.getEmail()) && employe.getMotDePasse().equals(user.getMotDePasse()) && (user.getCategorie().equals("E"))) {
                    System.out.println("Connection ok !\n");
                    choix = affichageMenuExpert(societe, user);

                    gestionMenuExpert(societe, choix, user);

                } else if (employe.getEmail().equals(user.getEmail()) && employe.getMotDePasse().equals(user.getMotDePasse())
                                && ((user.getCategorie().equals("S")) || (user.getCategorie().equals("A")))) {
                    System.out.println("Connection ok !\n");
                    choix = affichageMenuSeniorApprenti(societe, user);
                    gestionMenuSeniorAppreti(societe, choix, user);

                } else {
                    System.err.println("Email ou Mot de passe INCORRECT ! ");
                    choix = affichageMenuLancement(societe);
                    gestionMenuAcceuil(societe, choix);

                }
            } catch (NullPointerException e) {
                System.out.print("Vous vous �tes tromp�");
                choix = affichageMenuLancement(societe);
            }
            return choix;
        }

        /**
         * Permet de gerer le menu d'acceuil
         * 
         * @param societe du user
         * @param choix du user
         */
        public static void gestionMenuAcceuil(final Societe societe, final String choix) {
            String lettreChoisie = choix;

            do {
                switch (lettreChoisie) {

                    case "A" :
                        /** Se connecter */
                        System.out.println("-------------------------------    CONNEXION   -------------------------------");
                        System.out.println("Saisissez votre adresse mail");
                        String login = ScannerUtil.lire();
                        System.out.println("Saisissez votre mot de passe");
                        String motDePasse = ScannerUtil.lire();
                        Employe user = new Employe(login, motDePasse);
                        connection(societe, user);

                        break;
                    case "B" :
                        /** Afficher la liste des op�ations en cours */
                        System.out.println("-------------------------------    LISTE DES OPERATIONS EN COURS   -------------------------------");

                        for (Operation operation : Societe.afficheListeDesNomsOperationsOrderByName(Societe.listeDesOperationsEnCours)) {
                            System.out.println("Operation :\n   id :" + operation.getIdOperation() + "\n   Intitul� : " + operation.getIntitule() + "\n   Client : " + operation.getClient().getNom()
                                            + "\n   Employe : " + operation.getEmploye().getNom());
                        }

                        lettreChoisie = affichageMenuLancement(societe);
                        break;
                    case "C" :
                        /** Afficher la liste des op�rations termin�es */
                        System.out.println("-------------------------------    LISTE DES OPERATIONS TERMINEE   -------------------------------");

                        for (Operation operation : Societe.afficheListeDesNomsOperationsOrderByName(Societe.listeDesOperationsTerminee)) {
                            System.out.println("Operation :\n   id :" + operation.getIdOperation() + "\n   Intitul� : " + operation.getIntitule() + "\n   Client : " + operation.getClient().getNom()
                                            + "\n   Employe : " + operation.getEmploye().getNom());
                        }

                        lettreChoisie = affichageMenuLancement(societe);
                        break;
                    default :
                        while (!lettreChoisie.equals("A") && !lettreChoisie.equals("B") && !lettreChoisie.equals("C") && !lettreChoisie.equals("Q")) {
                            System.err.println("ERREUR : Veuillez saisir une lettre majuscule pour votre choix");
                            lettreChoisie = ScannerUtil.lire();
                        }
                        break;

                }
            } while (!lettreChoisie.equals("Q"));

            /** Quitter application */
            System.out.println("Vous etes sorti de l'application ! ");
            System.exit(0);

        }

        /**
         * Permet de gerer Menu Expert
         * 
         * @param societe du user
         * @param choix du user
         * @param user connect�
         */
        public static void gestionMenuExpert(final Societe societe, final String choix, Employe user) {
            String lettreChoisie = choix;

            do {
                switch (lettreChoisie) {
                    case "A" :
                        /** Ajouter un employ� */
                        System.out.println("-------------------------------    AJOUTER UN EMPLOYE   -------------------------------");

                        Societe.ajouterEmploye();

                        lettreChoisie = affichageMenuExpert(societe, user);
                        break;
                    case "B" :
                        /** Consulter le chiffre d'affaire */
                        System.out.println("-------------------------------    CONSULTER LE CHIFFRE D'AFFAIRE   -------------------------------");

                        System.out.println("Le chiffre d'affaire des operations termin�es est : " + Societe.chiffreAffaire);
                        System.out.println("Le chiffre d'affaire des operations en attente de paiement est  : " + Societe.paiementEnAttenteDeFinDeTravaux);

                        lettreChoisie = affichageMenuExpert(societe, user);
                        break;
                    case "C" :
                        /** Consulter la liste de mes op�rations termin�es */
                        System.out.println("-------------------------------    CONSULTER MES OPERATIONS TERMINEES   -------------------------------");

                        final ArrayList<Operation> listOperationTerUser = PrendreOperation.findOperationTerUserById(user);
                        consulterOperationTerminee(listOperationTerUser);

                        lettreChoisie = affichageMenuExpert(societe, user);

                        break;
                    case "D" :
                        /** Consulter la liste de mes op�rations en cours */
                        System.out.println("-------------------------------    CONSULTER MES OPERATIONS EN COURS  -------------------------------");

                        final ArrayList<Operation> listOperationEnCoursUser = PrendreOperation.findOperationEnCoursUserById(user);
                        consulterOperationEnCours(listOperationEnCoursUser);

                        lettreChoisie = affichageMenuExpert(societe, user);
                        break;
                    case "E" :
                        /** Prendre operation */
                        System.out.println("-------------------------------    PRENDRE OPERATION   -------------------------------");

                        enregistrerOperationClient(user);

                        lettreChoisie = affichageMenuExpert(societe, user);
                        break;
                    case "F" :
                        /** Terminer operation */
                        System.out.println("-------------------------------    TERMINER OPERATION   -------------------------------");

                        enregistrerManoeuvreTerminee(user);

                        lettreChoisie = affichageMenuExpert(societe, user);
                        break;

                    default :
                        while (!lettreChoisie.equals("A") && !lettreChoisie.equals("B") && !lettreChoisie.equals("C") && !lettreChoisie.equals("D") && !lettreChoisie.equals("E") && !choix.equals("F")
                                        && !lettreChoisie.equals("Q")) {
                            System.err.println("ERREUR : Veuillez saisir une lettre majuscule pour votre choix");
                            lettreChoisie = ScannerUtil.lire();
                        }
                        break;

                }
            } while (!lettreChoisie.equals("Q"));

            /** Quitter le menu expert */
            System.out.println("Vous �tes deconnect� ! ");
            user = null;
            lettreChoisie = affichageMenuLancement(societe);
            gestionMenuAcceuil(societe, lettreChoisie);
        }

        /**
         * Permet de gerer Menu des Employ�s Seniors et Apprentis
         * 
         * @param societe du user
         * @param choix du user
         * @param user connect�
         */
        public static void gestionMenuSeniorAppreti(final Societe societe, final String choix, Employe user) {
            String lettreChoisie = choix;

            do {
                switch (lettreChoisie) {
                    case "A" :
                        /** Prendre operation */
                        System.out.println("-------------------------------    PRENDRE OPERATION   -------------------------------");

                        enregistrerOperationClient(user);

                        lettreChoisie = affichageMenuSeniorApprenti(societe, user);
                        break;
                    case "B" :
                        /** Terminer operation */
                        System.out.println("-------------------------------    TERMINER OPERATION   -------------------------------");

                        enregistrerManoeuvreTerminee(user);

                        lettreChoisie = affichageMenuSeniorApprenti(societe, user);
                        break;
                    case "C" :
                        /** Consulter la liste de mes op�rations en cours */
                        System.out.println("-------------------------------    CONSULTER MES OPERATIONS EN COURS  -------------------------------");

                        final ArrayList<Operation> listOperationEnCoursUser = PrendreOperation.findOperationEnCoursUserById(user);
                        consulterOperationEnCours(listOperationEnCoursUser);

                        lettreChoisie = affichageMenuSeniorApprenti(societe, user);
                        break;
                    case "D" :
                        /** Consulter la liste de mes op�rations termin�es */
                        System.out.println("-------------------------------    CONSULTER MES OPERATIONS TERMINEE  -------------------------------");

                        final ArrayList<Operation> listOperationTerUser = PrendreOperation.findOperationTerUserById(user);
                        consulterOperationTerminee(listOperationTerUser);

                        lettreChoisie = affichageMenuSeniorApprenti(societe, user);
                        break;

                    default :
                        while (!lettreChoisie.equals("A") && !lettreChoisie.equals("B") && !lettreChoisie.equals("C") && !lettreChoisie.equals("D") && !lettreChoisie.equals("Q")) {
                            System.err.println("ERREUR : Veuillez saisir une lettre majuscule pour votre choix");
                            lettreChoisie = ScannerUtil.lire();
                        }
                        break;
                }
            } while (!lettreChoisie.equals("Q"));

            /** Quitter menu Senior/Apprenti */
            System.out.println("Vous �tes deconnect� ! ");
            user = null;
            lettreChoisie = affichageMenuLancement(societe);
            gestionMenuAcceuil(societe, lettreChoisie);
        }

        /**
         * Permet de cloturer une Manoeuvre en terminer
         * 
         * @param user connect�
         */
        public static void enregistrerManoeuvreTerminee(final Employe user) {
            Operation operation = new Operation();
            Integer id = 0;
            do {
                try {
                    System.out.println("Entrez l'id de l'operation : ");
                    id = Integer.valueOf(ScannerUtil.lire());
                } catch (NumberFormatException e) {
                    while (!id.getClass().isAssignableFrom(Integer.class)) {
                        System.out.println("Attention l'id ne peut �tre qu'un entier");
                        System.out.println("Entrez l'id de l'operation : ");
                        id = Integer.valueOf(ScannerUtil.lire());
                        // throw new NumberFormatException("Attention, l'id ne peut �tre que un entier
                        // !");
                    }
                } catch (Exception exception) {
                    System.out.println("Attention l'id ne peut �tre qu'un entier");
                    System.out.println("Entrez l'id de l'operation : ");
                    id = Integer.valueOf(ScannerUtil.lire());
                }
                operation = PrendreOperation.findOperationById(id);

            } while (operation == null);
            PrendreOperation.terminerOperation(user, operation);
            System.out.println("Op�ration clotur�e !");
        }

        /**
         * Permet d'enregistrer la prise d'une operation d'un client
         * 
         * @param user connect�
         */
        public static void enregistrerOperationClient(final Employe user) {
            System.out.println("Entrez le nom du client :");
            String nom = ScannerUtil.lire();
            System.out.println("Entrez le pr�nom du client :");
            String prenom = ScannerUtil.lire();
            Client client = enregistrerClient(nom, prenom);
            Operation operation = enregisterOperation(client, user);

            PrendreOperation.prendreOperation(user, operation);
        }

        /**
         * Permet de consulter les operations termin�es d'un user
         * 
         * @param listOperationTerUser liste des operations termin�es
         */
        public static void consulterOperationTerminee(final ArrayList<Operation> listOperationTerUser) {
            if (listOperationTerUser.equals(null)) {
                System.out.println("Toutes les op�rations sont en cours");
            } else {
                for (Operation operation : listOperationTerUser) {
                    System.out.println("Operation :\n   id :" + operation.getIdOperation() + "\n   Intitul� : " + operation.getIntitule() + "\n   Client : " + operation.getClient().getNom()
                                    + "\n   Employe : " + operation.getEmploye().getNom());
                }
            }
        }

        /**
         * Permet de consulter les operations en cours d'un user
         * 
         * @param listOperationEnCoursUser liste des operations en cours
         */
        public static void consulterOperationEnCours(final ArrayList<Operation> listOperationEnCoursUser) {
            if (listOperationEnCoursUser.equals(null)) {
                System.out.println("Toutes les op�rations sont termin�es !");
            } else {
                for (Operation operation : listOperationEnCoursUser) {
                    System.out.println("Operation :\n   id :" + operation.getIdOperation() + "\n   Intitul� : " + operation.getIntitule() + "\n   Client : " + operation.getClient().getNom()
                                    + "\n   Employe : " + operation.getEmploye().getNom());
                }
            }
        }

        /**
         * Permet d'enregistrer une operation
         * 
         * @param client de l'op�ration
         * @param user connect�
         * @return operation enregistr�e
         */
        public static Operation enregisterOperation(final Client client, final Employe user) {
            Operation operation = new Operation();
            System.out.println("Intitul� Op�ration : ");
            operation.setIntitule(ScannerUtil.lire());

            System.out.println("Type de manoeuvre (G)rande/(M)oyenne/(P)etite: ");
            String choix = ScannerUtil.lire();

            while (!choix.equals("G") && !choix.equals("M") && !choix.equals("P")) {
                System.err.println("Veuillez saisir G, M ou P ");
                System.out.println("Type de manoeuvre (G)rande/(M)oyenne/(P)etite: ");
                choix = ScannerUtil.lire();
            }
            operation.setTypeManoeuvre(ScannerUtil.lire());
            operation.setClient(client);
            operation.setStatut("EC");
            return operation;
        }

        /**
         * Permet d'enregistrer un client
         * 
         * @param nom du client
         * @param prenom du client
         * @return
         */
        public static Client enregistrerClient(final String nom, final String prenom) {
            Client client = PrendreOperation.ifClientExist(nom, prenom);
            if (client == null) {
                try {
                    System.out.println("Entrez l'id du nouveau client : ");
                    int id = Integer.valueOf(ScannerUtil.lire());
                    client = new Client(id, nom, prenom, "C");
                } catch (NumberFormatException e) {
                    System.err.println("Attention l'id ne peut �tre qu'un entier");
                    System.out.println("Entrez id client : ");
                    client.setId(Integer.valueOf(ScannerUtil.lire()));
                }

            }
            return client;
        }

        /**
         * Permet de lancer l'application
         * 
         * @param args tableau d'arguments
         */
        public static void main(final String[] args) {
            ScannerUtil.ouvrir();

            Societe societe = new Societe(58422, "NETTOYAGE PROPAR");
            initSociete(societe);

            String choix = affichageMenuLancement(societe);
            gestionMenuAcceuil(societe, choix);

            ScannerUtil.fermer();
        }

    }

}

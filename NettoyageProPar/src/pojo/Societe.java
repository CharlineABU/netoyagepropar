/**
 * POJO du projet
 */
package pojo;

import java.util.ArrayList;

import gestion.PrendreOperation;
import gestion.ScannerUtil;

/**
 * Objet Societe
 * 
 * @author Charline
 *
 */
public class Societe {

    /**
     * Identifiant de la soci�te
     */
    private int                               idSociete;
    /**
     * Nom de la societe
     */
    private String                            nom;
    /**
     * Liste de toutes les employ�s
     */
    public static ArrayList<Employe>          listeDesEmployes                = new ArrayList<Employe>();
    /**
     * Liste de tous les clients qui ont fait trait�es une operation dans la societe
     */
    public static ArrayList<Client>           listeDesClients                 = new ArrayList<Client>();
    /**
     * Liste de toutes les op�rations en cours
     */
    public static ArrayList<Operation>        listeDesOperationsEnCours       = new ArrayList<Operation>();
    /**
     * Liste de toutes les op�tations termin�es
     */
    public static ArrayList<Operation>        listeDesOperationsTerminee      = new ArrayList<Operation>();
    /**
     */
    public static ArrayList<PrendreOperation> listeDesOperationsEnregistrees  = new ArrayList<PrendreOperation>();
    /**
     * op�rations termin�es
     */
    public static double                      chiffreAffaire                  = 0;
    /**
     * Chiffre d'affaires des op�rations en cours
     */
    public static double                      paiementEnAttenteDeFinDeTravaux = 0;

    /**
     * Constructeur par defaut
     */
    public Societe() {
        super();
    }

    /**
     * Construteur : permet de construire une soci�te par son nonm et son id
     * 
     * @param idSociete de la societe
     * @param nom de la societe
     */
    public Societe(final int idSociete, final String nom) {
        super();
        this.idSociete = idSociete;
        this.nom = nom;
    }

    /**
     * Permet de lister tous les prenoms associ�e � un meme nom
     * 
     * @param nom de la societe
     * @param listeDesNomsClients de la societe
     * @param listeDesPrenomsClients de la societe
     * @return listePrenomsClientSameNom liste des prenoms des clients qui ont le meme nom
     */
    public static ArrayList<String> findPrenomsClientSameNom(final String nom, final ArrayList<String> listeDesNomsClients, final ArrayList<String> listeDesPrenomsClients) {
        System.out.println("hello");
        ArrayList<String> listePrenomsClientSameNom = new ArrayList<String>();
        for (int i = 0; i < listeDesNomsClients.size(); i++) {
            if (listeDesNomsClients.get(i).equals(nom)) {
                listePrenomsClientSameNom.add(listeDesPrenomsClients.get(i));
            }
        }
        return listePrenomsClientSameNom;
    }

    /**
     * Permet d'afficher une liste d'operation tri� par noms des clients
     * 
     * @param operations liste des operations � trier
     * @return listeDesOperationsTrieeParNomEtPrenom liste Des Operations Tri�e Par Nom Et Prenom
     */
    public static ArrayList<Operation> afficheListeDesNomsOperationsOrderByName(final ArrayList<Operation> operations) {
        ArrayList<String> listeDesNomsClients = new ArrayList<String>();
        ArrayList<String> listeDesPrenomsClients = new ArrayList<String>();
        ArrayList<String> listeDesNomsTriee = new ArrayList<String>();
        ArrayList<Operation> listeDesOperationsTrieeParNomEtPrenom = new ArrayList<Operation>();

        for (Operation operation : operations) {
            listeDesNomsClients.add(operation.getClient().getNom());
            listeDesNomsTriee.add(operation.getClient().getNom());
            listeDesPrenomsClients.add(operation.getClient().getPrenom());
        }

        listeDesNomsTriee.sort(String::compareToIgnoreCase);

        /** tri par prenom si m�me nom */
        for (int i = 0; i < listeDesNomsTriee.size() - 1; i++) {
            String nom1 = listeDesNomsTriee.get(i);
            String nom2 = listeDesNomsTriee.get(i + 1);
            if (i < listeDesNomsTriee.size() - 1) {
                if (nom1.equals(nom2)) {
                    ArrayList<String> listePrenomsClientSameNom = findPrenomsClientSameNom(nom1, listeDesNomsClients, listeDesPrenomsClients);
                    listePrenomsClientSameNom.sort(String::compareToIgnoreCase);

                    for (String prenom : listePrenomsClientSameNom) {
                        for (int j = 0; j < operations.size(); j++) {
                            if ((nom1.equals(operations.get(j).getClient().getNom()) && (prenom.equals(operations.get(j).getClient().getPrenom())))) {
                                listeDesOperationsTrieeParNomEtPrenom.add(operations.get(j));
                                break;
                            }
                        }
                    }

                } else {
                    for (int k = 0; k < operations.size(); k++) {
                        if (nom1.equals(operations.get(k).getClient().getNom())) {
                            listeDesOperationsTrieeParNomEtPrenom.add(operations.get(k));
                            break;
                        }
                    }
                }
            }
            if (i == (listeDesNomsTriee.size() - 2)) {
                for (int l = 0; l < operations.size(); l++) {
                    if (nom2.equals(operations.get(l).getClient().getNom())) {
                        listeDesOperationsTrieeParNomEtPrenom.add(operations.get(l));
                        break;
                    }
                }

            }

        }
        return listeDesOperationsTrieeParNomEtPrenom;

    }

    /**
     * Permet d'ajouter un employ�
     * 
     * @param id d'un employ�
     * @param nom d'un employ�
     * @param prenom d'un employ�
     * @param categorie d'un employ�
     * @param email d'un employ�
     * @param motDePasse d'un employ�
     */
    public static void ajouterEmploye() {

        Employe employe = new Employe();
        try {
            System.out.println("Entrez id employe : ");
            employe.setId(Integer.valueOf(ScannerUtil.lire()));
        } catch (NumberFormatException e) {
            System.out.println("Attention l'id ne peut �tre qu'un entier");
            System.out.println("Entrez id employe : ");
            employe.setId(Integer.valueOf(ScannerUtil.lire()));
        } catch (Exception e) {
            System.out.println("Attention l'id ne peut �tre qu'un entier");
            System.out.println("Entrez id employe : ");
            employe.setId(Integer.valueOf(ScannerUtil.lire()));
        }

        System.out.println("Entrez nom employ� : ");
        employe.setNom(ScannerUtil.lire());
        System.out.println("Entrez pr�nom employ� : ");
        employe.setPrenom(ScannerUtil.lire());
        System.out.println("Entrez cat�gorie employ�  (E)xpert/(S)enior/(A)pprenti : ");
        employe.setCategorie(ScannerUtil.lire());
        System.out.println("Entrez email employ� : ");
        employe.setEmail((ScannerUtil.lire()));
        System.out.println("Entrez mot de passe employ� : ");
        employe.setMotDePasse(ScannerUtil.lire());

        Societe.listeDesEmployes.add(employe);

        System.out.println("Employ� enregistr� !");
    }

    @Override
    public String toString() {
        return "Societe \n   id Societe :\t <" + idSociete + ">\n   Nom Societe : " + nom + "\n   Liste des Employes : " + Societe.listeDesEmployes.toString() + "\n   Liste des Clients : "
                        + Societe.listeDesClients.toString() + "\n   Liste des Operations en cours : " + Societe.listeDesOperationsEnCours.toString() + "\n   Liste des Operations termin�es : "
                        + Societe.listeDesOperationsTerminee.toString() + "\n   Chiffre d'Affaire : " + Societe.chiffreAffaire + "\n   Paiement en attente de fin de travaux : "
                        + Societe.paiementEnAttenteDeFinDeTravaux;
    }

    /**
     * @return the idSociete
     */
    public int getIdSociete() {
        return idSociete;
    }

    /**
     * @param idSociete the idSociete to set
     */
    public void setIdSociete(final int idSociete) {
        this.idSociete = idSociete;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(final String nom) {
        this.nom = nom;
    }

    /**
     * @return the listeDesEmployes
     */
    public static ArrayList<Employe> getListeDesEmployes() {
        return listeDesEmployes;
    }

    /**
     * @param listeDesEmployes the listeDesEmployes to set
     */
    public static void setListeDesEmployes(final ArrayList<Employe> listeDesEmployes) {
        Societe.listeDesEmployes = listeDesEmployes;
    }

    /**
     * @return the listeDesClients
     */
    public static ArrayList<Client> getListeDesClients() {
        return listeDesClients;
    }

    /**
     * @param listeDesClients the listeDesClients to set
     */
    public static void setListeDesClients(final ArrayList<Client> listeDesClients) {
        Societe.listeDesClients = listeDesClients;
    }

    /**
     * @return the listeDesOperationsEnCours
     */
    public static ArrayList<Operation> getListeDesOperationsEnCours() {
        return listeDesOperationsEnCours;
    }

    /**
     * @param listeDesOperationsEnCours the listeDesOperationsEnCours to set
     */
    public static void setListeDesOperationsEnCours(final ArrayList<Operation> listeDesOperationsEnCours) {
        Societe.listeDesOperationsEnCours = listeDesOperationsEnCours;
    }

    /**
     * @return the listeDesOperationsTerminee
     */
    public static ArrayList<Operation> getListeDesOperationsTerminee() {
        return listeDesOperationsTerminee;
    }

    /**
     * @param listeDesOperationsTerminee the listeDesOperationsTerminee to set
     */
    public static void setListeDesOperationsTerminee(final ArrayList<Operation> listeDesOperationsTerminee) {
        Societe.listeDesOperationsTerminee = listeDesOperationsTerminee;
    }

    /**
     * @return
     */
    public static ArrayList<PrendreOperation> getListeDesOperationsEnregistrees() {
        return listeDesOperationsEnregistrees;
    }

    /**
     * @param listeDesOperationsEnregistrees
     */
    public static void setListeDesOperationsEnregistrees(final ArrayList<PrendreOperation> listeDesOperationsEnregistrees) {
        Societe.listeDesOperationsEnregistrees = listeDesOperationsEnregistrees;
    }

    /**
     * @return
     */
    public static double getPaiementEnAttenteDeFinDeTravaux() {
        return paiementEnAttenteDeFinDeTravaux;
    }

    /**
     * @param paiementEnAttenteDeFinDeTravaux
     */
    public static void setPaiementEnAttenteDeFinDeTravaux(final double paiementEnAttenteDeFinDeTravaux) {
        Societe.paiementEnAttenteDeFinDeTravaux = paiementEnAttenteDeFinDeTravaux;
    }

    /**
     * @return the chiffreAffaire
     */
    public static double getChiffreAffaire() {
        return chiffreAffaire;
    }

    /**
     * @param chiffreAffaire the chiffreAffaire to set
     */
    public static void setChiffreAffaire(final double chiffreAffaire) {
        Societe.chiffreAffaire = chiffreAffaire;
    }

}

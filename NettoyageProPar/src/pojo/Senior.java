/**
 * POJO du projet
 */
package pojo;

/**
 * Objet Senior qui herite de Employe
 * 
 * @author Charline
 *
 */
public class Senior extends Employe {

    /**
     * Constructeur par defaut
     */
    public Senior() {
        super();
    }

    /**
     * Permet d'initialiser un Senior
     * 
     * @param id d'un senior
     * @param nom d'un senior
     * @param prenom d'un senior
     * @param categorie d'un senior
     * @param email d'un senior
     * @param motDePasse d'un senior
     * @return senior intanci�
     */
    public static Senior initSenior(final int id, final String nom, final String prenom, final String categorie, final String email, final String motDePasse) {
        Senior senior = new Senior();
        senior.setId(id);
        senior.setNom(nom);
        senior.setPrenom(prenom);
        senior.setCategorie(categorie);
        senior.setEmail(email);
        senior.setMotDePasse(motDePasse);
        return senior;
    }

    /**
     * Permet d'initaliser un Senior avec info employ�
     * 
     * @param employe
     * @return senior initialis�
     */
    public static Senior initSenior(final Employe employe) {
        Senior senior = new Senior();
        senior.setId(employe.getId());
        senior.setNom(employe.getNom());
        senior.setPrenom(employe.getPrenom());
        senior.setCategorie(employe.getCategorie());
        senior.setEmail(employe.getEmail());
        senior.setMotDePasse(employe.getMotDePasse());
        return senior;
    }

}

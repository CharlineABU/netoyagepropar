/**
 * POJO du projet
 */
package pojo;

/**
 * Objet Personne
 * 
 * @author 59013-07-04
 *
 */
public class Personne {
    /**
     * Identifiant d'une personne
     */
    private int    id;

    /**
     * Nom d'une personne
     */
    private String nom;
    /**
     * Prenom d'une personne
     */
    private String prenom;
    /**
     * Categorie d'une personne : (E)xpert, (S)enior, (A)pprenti, (C)lient
     */
    private String categorie;

    /**
     * constructeur par defaut
     */
    public Personne() {
        super();
    }

    /**
     * Constructeur : permet d'instancier une personne avec ses attributs
     * 
     * @param id d'une personne
     * @param nom d'une personne
     * @param prenom d'une personne
     * @param categorie d'une personne
     */
    public Personne(final int id, final String nom, final String prenom, final String categorie) {
        super();
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.categorie = categorie;

    }

    /**
     * permet d'afficher le nom et prenom d'une personne
     * 
     * @param personne � consulter
     */
    public static void afficherPersonne(final Personne personne) {
        System.out.println("\tnom : " + personne.getNom() + "\n\tprenom : " + personne.getPrenom());
    }

    @Override
    public String toString() {
        return "Personne : \n   id : " + id + "\n   nom : " + nom + "\n   prenom : " + prenom + "\n categorie : " + categorie + "\n";
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * @return the nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(final String nom) {
        this.nom = nom;
    }

    /**
     * @return the prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * @param prenom the prenom to set
     */
    public void setPrenom(final String prenom) {
        this.prenom = prenom;
    }

    /**
     * @return the categorie
     */
    public String getCategorie() {
        return categorie;
    }

    /**
     * @param categorie the categorie to set
     */
    public void setCategorie(final String categorie) {
        this.categorie = categorie;
    }

}

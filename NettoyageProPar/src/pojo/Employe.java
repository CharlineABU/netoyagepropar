package pojo;

/**
 * Objet Employe qui herite de Personne
 * 
 * @author Charline
 *
 */
/**
 * @author Charline
 *
 */
/**
 * @author Charline
 *
 */
public class Employe extends Personne {
    /**
     * Email d'une personne
     */
    private String email;
    /**
     * Mot de passe d'une personne
     */
    private String motDePasse;
    /**
     * Nombre d'operation prise
     */
    private int    nombreOperationPrise;
    /**
     * Compteur static permettant l'auto-incr�mentation de le nombre d'op�ration prise
     */
    public int     compteurNombreOperation;

    /**
     * Constructeur par defaut
     */
    public Employe() {
        super();
    }

    /**
     * Constructeur : permet d'instancier un employe avec son email et son mot de passe
     * 
     * @param email de l'employe
     * @param motDePasse de l'employe
     */
    public Employe(String email, String motDePasse) {
        super();
        this.email = email;
        this.motDePasse = motDePasse;
    }

    /**
     * Constructeur : permet d'instancier un employe avec ses attributs
     * 
     * @param email de l'employe
     * @param motDePasse de l'employe
     * @param nombreOperationPrise de l'employe
     * @param compteurNombreOperation de l'employe
     */
    public Employe(String email, String motDePasse, int nombreOperationPrise, int compteurNombreOperation) {
        this.email = email;
        this.motDePasse = motDePasse;
        this.nombreOperationPrise = nombreOperationPrise;
        this.compteurNombreOperation = compteurNombreOperation;
    }

    /**
     * Constructeur : permet d'instancier un employe avec ses attributs
     * 
     * @param id de l'employe
     * @param nom de l'employe
     * @param prenom de l'employe
     * @param categorie de l'employe
     * @param email de l'employe
     * @param motDePasse de l'employe
     * @param nombreOperationPrise de l'employe
     * @param compteurNombreOperation de l'employe
     */
    public Employe(final int id, final String nom, final String prenom, final String categorie, final String email, final String motDePasse, int nombreOperationPrise, int compteurNombreOperation) {
        super(id, nom, prenom, categorie);
        this.email = email;
        this.motDePasse = motDePasse;
    }

    /**
     * Permet d'initaliser un Employe avec info personne
     * 
     * @param personne avec informations � recup�rer
     * @return employe initialis�
     */
    public static Employe initEmploye(final Personne personne) {
        Employe employe = new Employe();
        employe.setId(personne.getId());
        employe.setNom(personne.getNom());
        employe.setPrenom(personne.getPrenom());
        employe.setCategorie(personne.getCategorie());
        return employe;
    }

    @Override
    public String toString() {
        return super.toString() + "Employe [email=" + email + ", motDePasse=" + motDePasse + "]";
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the motDePasse
     */
    public String getMotDePasse() {
        return motDePasse;
    }

    /**
     * @param motDePasse the motDePasse to set
     */
    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    /**
     * @return the nombreOperationPrise
     */
    public int getNombreOperationPrise() {
        return nombreOperationPrise;
    }

    /**
     * @param nombreOperationPrise the nombreOperationPrise to set
     */
    public void setNombreOperationPrise(final int nombreOperationPrise) {
        this.nombreOperationPrise = nombreOperationPrise;
    }

    /**
     * @return the compteurNombreOperation
     */
    public int getCompteurNombreOperation() {
        return compteurNombreOperation;
    }

    /**
     * @param compteurNombreOperation the compteurNombreOperation to set
     */
    public void setCompteurNombreOperation(final int compteurNombreOperation) {
        this.compteurNombreOperation = compteurNombreOperation;
    }

}

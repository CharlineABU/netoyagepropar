package pojo;

/**
 * Objet Operation. Les op�rations sont de trois type : Grande, Moyenne ou Petite manoeuvre.
 * 
 * @author Charline
 *
 */
public class Operation {
    /**
     * Identifiant d'une op�ration
     */
    private int        idOperation;
    /**
     * Compteur static permettant l'auto-incr�mentation de l'id
     */
    private static int compteurId;
    /**
     * Intitul� d'une op�ration
     */
    private String     intitule;

    /**
     * Client de l'operation
     */
    private Client     client  = new Client();
    /**
     * Employe qui traite l'operation
     */
    private Employe    employe = new Employe();
    /**
     * Type d'une Manoeuvre : (G)rande/(M)oyenne/(P)etite
     */
    private String     typeManoeuvre;
    /**
     * Statut de l'op�ration : EC (en cours) ou TER (termin�e)
     */
    private String     statut;
    /**
     * Prix d'une op�ration
     */
    private double     prix;

    /**
     * Constructeur par defaut
     */
    public Operation() {
        Operation.compteurId++;
    }

    /**
     * 
     * Constructeur : permet d'instancier une op�ration avec ses attributs
     * 
     * @param idOperation d'une op�ration
     * @param intitule d'une op�ration
     * @param typeManoeuvre d'une op�ration
     * @param statut d'une op�ration
     * @param prix d'une op�ration
     */
    public Operation(final String intitule, final Client client, final Employe employe, final String typeManoeuvre, final String statut) {

        compteurId++;
        this.idOperation = compteurId;
        this.intitule = intitule;
        this.client = client;
        this.employe = employe;
        this.typeManoeuvre = typeManoeuvre;
        this.statut = statut;
        if (typeManoeuvre.equals("G")) {
            this.prix = 10000.00;
        } else if (typeManoeuvre.equals("M")) {
            this.prix = 2500.00;
        } else if (typeManoeuvre.equals("P")) {
            this.prix = 1000.00;
        } else {
            // TODO lever l'exception avec possiblit� correction
            // throws new ExceptionsGestionDesOperations("Erreur type Manoeuvre");
        }

    }

    @Override
    public String toString() {

        return "Operation :\nNom client de l'op�ration : \t" + client.getNom() + "\nPrenom client de l'op�ration :\t " + client.getPrenom() + "\nNom de l'employ� qui traite l'op�ration : \t"
                        + employe.getNom() + "\nPrenom de l'employ� qui traite l'op�ration :\t " + employe.getPrenom() + "\n   Id Operation : " + idOperation + "\n   Intitule = " + intitule
                        + "\n   Type Manoeuvre : " + typeManoeuvre + "\n   Statut : " + statut + "\n   Prix : " + prix;

    }

    /**
     * @return the idOperation
     */
    public int getIdOperation() {
        return idOperation;
    }

    /**
     * @param idOperation the idOperation to set
     */
    public void setIdOperation(final int idOperation) {
        this.idOperation = idOperation;
    }

    /**
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * @param intitule the intitule to set
     */
    public void setIntitule(final String intitule) {
        this.intitule = intitule;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(final Client client) {
        this.client = client;
    }

    /**
     * @return the employe
     */
    public Employe getEmploye() {
        return employe;
    }

    /**
     * @param employe the employe to set
     */
    public void setEmploye(final Employe employe) {
        this.employe = employe;
    }

    /**
     * @return the typeManoeuvre
     */
    public String getTypeManoeuvre() {
        return typeManoeuvre;
    }

    /**
     * @param typeManoeuvre the typeManoeuvre to set
     */
    public void setTypeManoeuvre(final String typeManoeuvre) {
        this.typeManoeuvre = typeManoeuvre;
    }

    /**
     * @return the statut
     */
    public String getStatut() {
        return statut;
    }

    /**
     * @param statut the statut to set
     */
    public void setStatut(final String statut) {
        this.statut = statut;
    }

    /**
     * @return the prix
     */
    public double getPrix() {
        return prix;
    }

    /**
     * @param prix the prix to set
     */
    public void setPrix(final double prix) {
        this.prix = prix;
    }

}

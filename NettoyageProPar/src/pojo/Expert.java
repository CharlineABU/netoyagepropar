/**
 * POJO du projet
 */
package pojo;

/**
 * Objet Expert qui herite de Employe
 * 
 * @author Charline
 *
 */
public class Expert extends Employe {

    /**
     * Constructeur par defaut
     */
    public Expert() {
        super();
    }

    /**
     * Permet d'initialiser un expert
     * 
     * @param id d'un expert
     * @param nom d'un expert
     * @param prenom d'un expert
     * @param categorie d'un expert
     * @param email d'un expert
     * @param motDePasse d'un expert
     * @return
     */
    public static Expert initExpert(final int id, final String nom, final String prenom, final String categorie, final String email, final String motDePasse) {
        Expert expert = new Expert();
        expert.setId(id);
        expert.setNom(nom);
        expert.setPrenom(prenom);
        expert.setCategorie(categorie);
        expert.setEmail(email);
        expert.setMotDePasse(motDePasse);
        return expert;
    }

    /**
     * Permet d'initaliser un Expert avec info employ�
     * 
     * @param employe avec informations � r�cuperer
     * @return expert initialis�
     */
    public static Expert initExpert(final Employe employe) {
        Expert expert = new Expert();
        expert.setId(employe.getId());
        expert.setNom(employe.getNom());
        expert.setPrenom(employe.getPrenom());
        expert.setCategorie(employe.getCategorie());
        expert.setEmail(employe.getEmail());
        expert.setMotDePasse(employe.getMotDePasse());
        return expert;
    }

}

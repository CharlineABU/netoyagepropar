/**
 * POJO du projet
 */
package pojo;

/**
 * Objet Apprenti qui herite de Employe
 * 
 * @author Charline
 *
 */
public class Apprenti extends Employe {

    /**
     * Instance d'une operation
     */
    private Operation operation;

    /**
     * Constructeur par defaut
     */
    public Apprenti() {
        super();
    }

    /**
     * Permet d'initialiser un Apprenti
     * 
     * @param id d'un apprenti
     * @param nom d'un apprenti
     * @param prenom d'un apprenti
     * @param categorie d'un apprenti
     * @param email d'un apprenti
     * @param motDePasse d'un apprenti
     * @return apprenti intanci�
     */
    public static Apprenti initApprenti(final int id, final String nom, final String prenom, final String categorie, final String email, final String motDePasse) {
        Apprenti apprenti = new Apprenti();
        apprenti.setId(id);
        apprenti.setNom(nom);
        apprenti.setPrenom(prenom);
        apprenti.setCategorie(categorie);
        apprenti.setEmail(email);
        apprenti.setMotDePasse(motDePasse);
        return apprenti;
    }

    /**
     * Permet d'initaliser un Apprenti avec info employ�
     * 
     * @param employe avec informations � recup�rer
     * @return apprenti initialis�
     */
    public static Apprenti initApprenti(final Employe employe) {
        Apprenti apprenti = new Apprenti();
        apprenti.setId(employe.getId());
        apprenti.setNom(employe.getNom());
        apprenti.setPrenom(employe.getPrenom());
        apprenti.setCategorie(employe.getCategorie());
        apprenti.setEmail(employe.getEmail());
        apprenti.setMotDePasse(employe.getMotDePasse());
        return apprenti;
    }

    @Override
    public String toString() {
        return super.toString() + "Apprenti [operation=" + operation + "]";
    }

    /**
     * @return the operation
     */
    public Operation getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(final Operation operation) {
        this.operation = operation;
    }

}
